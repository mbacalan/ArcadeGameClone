# Arcade Game Clone

A browser based arcade game (frogger) clone using JavaScript. Play at: https://pen-mb.github.io/ArcadeGameClone/

## The Plot

**After many long days, weeks and months, it is finally time for Princess Girl to enter her new age. Since she is called Princess Girl, she can't simply throw a party like a normal human being. Instead, she decides to rent a ship and go on a cruise, unaware of her fate...**

**Princess Girl gathers her closest friends and they all board the ship. Many milks bottles are emptied, many candy bars are melted. For reasons unknown, the ship's captain has disappeared and the ship has crashed into an unknown island, filled with giant bugs.**

**Will you save all five characters by guiding them safely to the sea or will you fail them in this simple task?**

## The Gameplay

The game features 5 different characters;

* ***The Boy***,
* ***The Pink Girl***,
* ***The Horn Girl***,
* ***The Cat Girl***,
* and of course, ***The Princess Girl***.

Using the arrow keys on your keyboard, navigate the characters through the bug infested island to the sea, without causing any collision between them.

*The Boy is always the first one to run - don't ask why. After he is safe, rest of the characters will follow in a random manner.*

## The Installation

Simply clone the repository and run ``index.html``.

## Notes

This project is built upon the base code provided by [Udacity](https://www.udacity.com/) under the Front-End Web Developer Nanodegree.
