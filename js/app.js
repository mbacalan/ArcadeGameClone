// Enemies our player must avoid
class Enemy {
  constructor(x, y) {
    // Variables applied to each of our instances go here,
    // we've provided one for you to get started
    // The image/sprite for our enemies, this uses
    // a helper we've provided to easily load images
    this.sprite = "images/enemy-bug.png";
    this.x = x;
    this.y = y;
    this.rate = this.randomRate();
  }
  // Set the rate to a random integer between min and max
  randomRate() {
    this.min = 175;
    this.max = 400;
    return this.rate = Math.floor(Math.random() * (this.max - this.min)) + this.min;
  }
  // Update the enemy's position, required method for game
  // Parameter: dt, a time delta between ticks
  update(dt) {
    // You should multiply any movement by the dt parameter
    // which will ensure the game runs at the same speed for
    // all computers.
    this.x = this.x + (dt * this.rate);
    if (this.x > 700) {
      this.x =- Math.random() * 170;
      this.rate = this.randomRate();
    }
  }
  // Draw the enemy on the screen, required method for game
  render() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
  }
}

// Now write your own player class
// This class requires an update(), render() and
// a handleInput() method.
class Player {
  constructor(x, y) {
    this.sprites = [
      "images/char-cat-girl.png",
      "images/char-horn-girl.png",
      "images/char-pink-girl.png",
      "images/char-princess-girl.png",
    ],
    this.sprite = "images/char-boy.png";
    this.x = x;
    this.y = y;
  }
  update(dt) {
    if (this.y < 60) {
      // Remove the current sprite from the sprites array
      // so each round is guaranteed to have a different sprite
      for(var i = this.sprites.length - 1; i >= 0; i--) {
        if (this.sprites[i] == this.sprite) {
          this.sprites.splice(i,1);
        }
      }
      // Reset the player position
      this.y = 400;
      // Get a random sprite
      this.sprite = this.sprites[Math.floor(Math.random() * this.sprites.length)];
    }
  }
  saved() {
    if (this.sprite === "images/char-boy.png") {
      document.querySelector("h1").innerHTML = "Save The Boy!";
    } else if (this.sprite === "images/char-pink-girl.png") {
      document.querySelector("h1").innerHTML = "Save The Pink Girl!";
    } else if (this.sprite === "images/char-horn-girl.png") {
      document.querySelector("h1").innerHTML = "Save The Horn Girl!";
    } else if (this.sprite === "images/char-cat-girl.png") {
      document.querySelector("h1").innerHTML = "Save The Cat Girl!";
    } else if (this.sprite === "images/char-princess-girl.png") {
      document.querySelector("h1").innerHTML = "Save The Princess Girl!";
    }
    // When there are no sprites left, finish the game
    if (!this.sprite) {
      document.querySelector("h1").innerHTML = "Congratulations!";
      document.querySelector("p").innerHTML = "You have saved everyone!";
    }
  }
  render() {
    ctx.drawImage(Resources.get(this.sprite), this.x, this.y);
  }
  handleInput(key) {
    switch (key) {
    case "up":
      if (this.y > 0) {
        this.y -= 85;
      }
      break;
    case "down":
      if (this.y < 400) {
        this.y += 85;
      }
      break;
    case "left":
      if (this.x > 0) {
        this.x -= 100;
      }
      break;
    case "right":
      if (this.x < 655) {
        this.x += 100;
      }
      break;
    }
  }
}

// Now instantiate your objects.
// Place all enemy objects in an array called allEnemies
// Place the player object in a variable called player
var enemy1 = new Enemy(100, 50);
var enemy2 = new Enemy(100, 135);
var enemy3 = new Enemy(100, 220);
var allEnemies = [enemy1, enemy2, enemy3];

var player = new Player(200, 400);

// This listens for key presses and sends the keys to your
// Player.handleInput() method. You don't need to modify this.
document.addEventListener("keyup", function (e) {
  var allowedKeys = {
    37: "left",
    38: "up",
    39: "right",
    40: "down"
  };

  player.handleInput(allowedKeys[e.keyCode]);
});
